class Task < ActiveRecord::Base
  include AASM

  belongs_to :assignee, class_name: 'User'
  belongs_to :list

  aasm column: 'status', whiny_transitions: false do
    state :incomplete, initial: true
    state :complete

    event :finish do
      transitions from: :incomplete, to: :complete
    end

    event :re_open do
      transitions from: :complete, to: :incomplete
    end
  end

  scope :ordered, -> { order('created_at DESC') }

  validates :title, presence: true
  validates :description, presence: true
  validates :assignee, presence: true
  validates :list, presence: true
end
