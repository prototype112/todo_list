class TasksController < ApplicationController
  before_action :authenticate_user!
  before_action :set_list, only: [:index, :new, :create]
  before_action :set_user, only: [:user_tasks, :create_for_user]
  before_action :set_task, only: [:show, :edit, :update, :destroy, :finish, :re_open]

  def index
    @title = 'Tasks'
    @tasks = @list.tasks.ordered
  end

  def user_tasks
    @title = 'Assigned Tasks'
    @filter = params[:filter] || 'incomplete'
    @tasks = if ['complete', 'all'].include? @filter
      @user.assigned_tasks.send(@filter).ordered
    else
      @user.assigned_tasks.incomplete.ordered
    end
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
    @title = 'View Task'
  end

  def new
    @title = 'New Task'
    @task = @list.tasks.new
  end

  # GET /tasks/1/edit
  def edit
    @title = 'Edit Task'
  end

  def create
    @task = @list.tasks.new(task_params)
    @task.assignee = current_user unless @task.assignee.present?

    respond_to do |format|
      if @task.save
        format.html { redirect_to @task.list, notice: 'Task was successfully created.' }
        format.json { render :show, status: :created, location: @task.list }
      else
        format.html { render :new }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
      format.js
    end
  end

  def create_for_user
    @task = @user.assigned_tasks.new(task_params)

    respond_to do |format|
      if @task.save
        format.html { redirect_to @task.list, notice: 'Task was successfully created.' }
        format.json { render :show, status: :created, location: @task.list }
      else
        format.html { render :new }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
      format.js
    end
  end

  # PATCH/PUT /tasks/1
  # PATCH/PUT /tasks/1.json
  def update
    respond_to do |format|
      if @task.update(task_params)
        format.html { redirect_to @task.list, notice: 'Task was successfully updated.' }
        format.json { render :show, status: :ok, location: @task.list }
      else
        format.html { render :edit }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    @list = @task.list
    @task.destroy
    respond_to do |format|
      format.html { redirect_to list_tasks_url(@list), notice: 'Task was successfully destroyed.' }
      format.json { head :no_content }
      format.js
    end
  end


  # PATCH/PUT /tasks/1
  def finish
    @task.finish
    @task.save
    respond_to do |format|
      format.html { redirect_to @task, notice: 'Task was successfully completed.' }
      if @task.complete?
        format.json { render :show, status: :ok, location: @task }
      else
        format.json { render json: { error: 'Task could not be completed' }, status: :unprocessable_entity }
      end
    end
  end

# PATCH/PUT /tasks/1
  def re_open
    @task.re_open
    @task.save
    respond_to do |format|
      format.html { redirect_to @task, notice: 'Task was successfully completed.' }
      if @task.incomplete?
        format.json { render :show, status: :ok, location: @task }
      else
        format.json { render json: { error: 'Task could not be re-opened' }, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task
      @task = Task.find(params[:id])
    end

    def set_list
      @list = List.find(params[:list_id])
    end

    def set_user
      @user = User.find(params[:user_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_params
      params.require(:task).permit(:title, :description, :due_date, :assignee_id, :list_id)
    end
end
