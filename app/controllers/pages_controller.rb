class PagesController < ApplicationController
  before_action :authenticate_user!

  def home
    redirect_to lists_path
  end
end
