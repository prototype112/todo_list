# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'turbolinks:load', ->
  $('[data-toggle="tooltip"]').tooltip();

  $('#date-field-container').datetimepicker
    format: 'YYYY-MM-DD HH:mm'

  $('#task-container').on 'change', '.finish-btn', (event) ->
    event.preventDefault()
    chk_box = $(this)
    task_id = chk_box.data('id')
    if this.checked
      chk_box.prop('checked', false)
      chk_box.attr('disabled', true)
      $.ajax
        url: "/tasks/#{task_id}/finish"
        method: 'GET'
        dataType: 'JSON'
        success: (data) ->
          chk_box.attr('disabled', false)
          chk_box.prop('checked', true)
          if $('#task-container').data('filter') == 'incomplete'
            chk_box.closest('.task-item').remove()
          else
            chk_box.closest('.task-item').addClass('complete-item')
        error: (data) ->
          alert(data.responseJSON['error'])
    else
      chk_box.prop('checked', true)
      chk_box.attr('disabled', true)
      $.ajax
        url: "/tasks/#{task_id}/re_open"
        method: 'GET'
        dataType: 'JSON'
        success: (data) ->
          chk_box.attr('disabled', false)
          chk_box.prop('checked', false)
          if $('#task-container').data('filter') == 'complete'
            chk_box.closest('.task-item').remove()
          else
            chk_box.closest('.task-item').removeClass('complete-item')
        error: (data) ->
          alert(data.responseJSON['error'])

  $('#task-container').on 'click', '.remote-task-remove-link', ->
    return confirm('Are you sure you want to delete this task?')

  $('.date > input').click ->
    $(this).next().click()
