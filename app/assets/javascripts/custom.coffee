window.fade_alert = ->
  $('.alert').fadeTo(500, 0).slideUp 500, ->
    $(this).remove()

$(document).on 'turbolinks:load', ->
  window.setTimeout(window.fade_alert, 3000)
