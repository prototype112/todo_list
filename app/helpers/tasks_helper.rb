module TasksHelper
  def class_for_task_item(task)
    'complete-item' if task.complete?
  end
end
