class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :title
      t.text :description
      t.datetime :due_date
      t.references :assignee, index: true, references: :users

      t.timestamps null: false
    end
  end
end
